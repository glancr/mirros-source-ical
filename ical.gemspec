# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'ical/version'
require 'json'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'ical'
  s.version     = Ical::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de'
  s.summary     = 'Source extension for mirr.OS one that integrates remote iCalendar files.'
  s.description = 'Access data from online calendars in iCal format. Supports both public and password-protected iCal links.'
  s.license     = 'MIT'
  s.metadata = { 'json' =>
                   {
                     type: 'sources',
                     title: {
                       enGb: 'iCal',
                       deDe: 'iCal',
                       frFr: 'iCal',
                       esEs: 'iCal',
                       plPl: 'iCal',
                       koKr: 'iCal'
                     },
                     description: {
                       enGb: s.description,
                       deDe: 'Greife auf Daten aus Online-Kalendern im iCal-Format zu. Unterstützt sowohl öffentliche als auch passwortgeschützte iCal-Links.',
                       frFr: 'Accéder aux données des calendriers en ligne au format iCal Prend en charge les liens iCal publics et protégés par un mot de passe.',
                       esEs: 'Acceder a los datos de los calendarios en línea en formato iCal Admite enlaces de iCal tanto públicos como protegidos por contraseña.',
                       plPl: 'Dostęp do danych z kalendarzy internetowych w formacie iCal Obsługuje zarówno publiczne, jak i chronione hasłem linki iCal.',
                       koKr: 'iCal 형식의 온라인 캘린더 데이터에 액세스하십시오. 공용 및 암호로 보호 된 iCal 링크를 모두 지원합니다.'
                     },
                     groups: %i[calendar reminder_list],
                     compatibility: '0.27.0'
                   }.to_json }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'httparty', '~> 0.20'
  s.add_dependency 'icalendar', '~> 2.4'
  s.add_dependency 'icalendar-recurrence', '~> 1.1'
  s.add_development_dependency 'rails', '~> 5.2'
end
