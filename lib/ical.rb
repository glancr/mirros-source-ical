# frozen_string_literal: true

require 'ical/engine'
require 'ical/fetcher'
require 'httparty'

module Ical
  # Implements mirr.OS data source hooks.
  class Hooks
    REFRESH_INTERVAL = '5m'

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @url = configuration['url']
      @auth = if configuration['urlRequiresAuth']
                {
                  basic_auth: {
                    username: configuration['user'],
                    password: configuration['password']
                  }
                }
              else
                {}
              end
      @fetcher = Fetcher.new(@url, @auth)
    end

    # Returns all calendar names found in the configured .ics file.
    # @return [String] all calendar names, separated by comma and space.
    def default_title
      @fetcher.calendars.map { |cal| cal[1] }.join(', ')
    end

    def configuration_valid?
      # TODO: Check if valid URL at all
      res = if @url.match?(/.icloud\.com/)
              # iCloud responds with 401/503 to HEAD requests
              HTTParty.get(@url, @auth)
            else
              HTTParty.head(@url, @auth)
            end
      res.success?
    rescue ArgumentError
      false
    end

    def list_sub_resources
      @fetcher.calendars
    end

    def fetch_data(group, sub_resources)
      records = []
      case group
      when 'calendar'
        sub_resources.each { |sub_resource| records << fetch_calendar(sub_resource) }
      when 'reminder_list'
        sub_resources.each { |sub_resource| records << fetch_reminder(sub_resource) }
      else
        Rails.logger.error "Invalid group #{group} for #{self.class}"
      end

      records
    end

    private


    # @param [Object] sub_resource UID of the calendar which should be fetched
    # @return [Ical::Calendar] An unsaved instance of Ical::Calendar with events attached
    def fetch_calendar(sub_resource)
      cal_ref = Calendar.includes(:events).find_or_initialize_by(
        id: sub_resource
      ) do |calendar|
        calendar.id = sub_resource
      end

      meta = @fetcher.calendar_meta(sub_resource)
      cal_ref.assign_attributes(
        name: meta[:name] || '',
        description: meta[:description] || '',
        color: meta[:color] || ''
      )

      events = @fetcher.events(sub_resource)
      current_uids = []

      events.each do |event|
        current_uids << event[:uid]
        attributes = {
          dtstart: event[:dtstart],
          dtend: event[:dtend],
          summary: event[:summary]&.force_encoding(Encoding::UTF_8),
          description: event[:description]&.force_encoding(Encoding::UTF_8),
          all_day: event[:all_day],
          several_days: event[:several_days],
          location: event[:location]&.force_encoding(Encoding::UTF_8)
        }
        cal_ref.update_or_insert_child(event[:uid], attributes)
      end

      # Delete all events that are no longer in the source
      cal_ref.events.each do |event|
        event.mark_for_destruction unless current_uids.include?(event.uid)
      end

      cal_ref
    end

    def fetch_reminder(sub_resource)
      reminder_list_ref = ReminderList.includes(:reminders).find_or_initialize_by(
        id: sub_resource
      ) do |reminder_list|
        reminder_list.id = sub_resource
      end

      meta = @fetcher.calendar_meta(sub_resource)
      reminder_list_ref.update(
        name: meta[:name]&.force_encoding(Encoding::UTF_8),
        description: meta[:description]&.force_encoding(Encoding::UTF_8),
        color: meta[:color]
      )

      reminders = @fetcher.reminders(sub_resource)
      current_uids = []

      reminders.each do |reminder|
        current_uids << reminder[:uid]
        reminder_list_ref.update_or_insert_child(reminder[:uid], reminder)
      end

      # Delete all events that are no longer in the source
      reminder_list_ref.reminders.each do |r|
        r.mark_for_destruction unless current_uids.include?(r.uid)
      end

      reminder_list_ref
    end
  end
end

